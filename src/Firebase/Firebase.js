import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyAFgom6jw1GQur7tUJNP_gcl7MvU7fDaXU",
  authDomain: "todos-d2cd7.firebaseapp.com",
  databaseURL: "https://todos-d2cd7.firebaseio.com",
  projectId: "todos-d2cd7",
  storageBucket: "todos-d2cd7.appspot.com",
  messagingSenderId: "303965733787",
  appId: "1:303965733787:web:bb0b122e79082190"
}

firebase.initializeApp(config);
firebase.firestore();

export default firebase;