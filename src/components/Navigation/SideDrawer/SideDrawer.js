import React, { useState } from 'react'
import styled from 'styled-components';

import Logo from '../../Logo/Logo';
import Hamburger from './Hamburger/Hamburger';
import NavItems from '../NavItems/NavItems';

const FixedWrapper = styled.div `
  position: fixed;
  z-index: 10; 
  background-color: var(--color-mainDark);
  padding: 0rem 2rem;
  top: 0;
  left: 0;
  width: 100%;
  height: 6rem;
  display: none;

  @media ${props => props.theme.mediaQueries.small}{
    display: flex;
  }
`;

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`;

const Menu = styled.div`
  width: 100%;
  background-color: var(--color-mainDark);
  height: 100vh;
  visibility: ${props => props.opened ? 'visible' : 'hidden'};
  transform: translateY(${props => (props.opened ? '0%' : '-100%')});
  transition: all 0.2s cubic-bezier(0.445, 0.05, 0.55, 0.95);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  display: none;

  @media ${props => props.theme.mediaQueries.small}{
    display: flex;
  }
`;

const SideDrawer = ({ loggedIn }) => {
  const [isOpened, setIsOpened] = useState(false);
  return (
    <>
      <FixedWrapper>
        <Wrapper>
            <Logo />
            <Hamburger opened={isOpened} clicked={() => setIsOpened(!isOpened)}/>
        </Wrapper>
      </FixedWrapper>
      <Menu opened={isOpened}>
        <NavItems mobile clicked={() => setIsOpened(!isOpened)} loggedIn={loggedIn}/>
      </Menu>
    </>
  )
}

export default SideDrawer;
