import React from 'react'
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import Backdrop from './Backdrop/Backdrop';

const WrapperModal = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: ${({opened}) => 
    // From the top animation...
    opened ? 'translate(-50%, -50%)' : 'translate(-50%, -150%)'};
  transition: all 0.1s;
  display:flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  z-index: 150;
  opacity: ${({opened}) => opened ? '1' : '0'};
  visibility: ${({opened}) => opened ? 'visible' : 'hidden'};
  width: 100%;
  max-width: 50rem;
  box-shadow: 0 .5rem 3.5rem var(--shadow);
  border-radius: 1rem;
  background-color: var(--color-mainLight);
`;

const InsideWrapper = styled.div`
  position: relative;
  width: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
  flex-direction: column; 
  padding: 4rem 3rem;
`;

const Modal = ({opened, close, children}) => {
  return ReactDOM.createPortal(
    <>  
    <Backdrop close={close} opened={opened}/>
    <WrapperModal opened={opened}>
      <InsideWrapper>
        {children}
      </InsideWrapper>
    </WrapperModal>
    </>,
    document.getElementById('root-modal'));
}

export default Modal
