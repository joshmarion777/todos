import React, {useEffect, useState} from 'react'
import { connect } from 'react-redux'
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';

import * as actions from '../../store/actions';

import { FormWrapper, StyledForm } from "../../hoc/layout/elements";
import Heading from '../../components/UI/Headings/Heading';
import Input from '../../components/UI/Forms/Input/Input';
import Button from '../../components/UI/Forms/Button/Button';
import Message from '../../components/UI/Message/Message';
import Modal from '../../components/Modal/Modal';

const MessageWrapper = styled.div`
  position: absolute;
  bottom: 3rem;
  width: 100%;
  padding: .5rem 2rem;
`;
const DeleteWrapper = styled.div`
  cursor: pointer;
  color: var(--color-errorRed);
  font-size: 1.3rem;
  font-weight: 700;
  transition: all 0.2s;
  margin-top: 2rem;
  &:hover{
    transform: translateY(-3px);
  }

  &:active{
    transform: translateY(2px);
  }
`;

const ButtonsWrapper = styled.div`
  display:flex;
  width: 100%;
  margin-bottom: 2rem;
  justify-content: space-around;
`;

const ProfileSchema = Yup.object().shape({
  firstName: Yup.string()
  .required('Your first name is required')
  .min(3, 'Too short...')
  .max(25, 'Too Long...'),
  lastName: Yup.string()
  .required('Your last name is required')
  .min(3, 'Too short...')
  .max(25, 'Too Long...'),
  email: Yup.string()
  .email('Invalid Email')
  .required('The email is required'),
  password: Yup.string()
  .min(8, 'The password is too short'),
  confirmPassword: Yup.string().when('password', {
    is: password => password.length > 0,
    then: Yup.string()
    .oneOf([Yup.ref('password'), null], `Password doesn't match` )
    .required('You need to confirm your password'),
  })
});

const Profile = 
  ({ firebase, editProfile, 
    loading, error, cleanUp, deleteUser, loadingDelete, errorDelete }) => {
  
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp])
  const [modalOpened, setModalOpened] = useState(false);
  // made this because I couldn't get the data as FB profile state 
  // is null or false at initial state..jus waiting to get the values.
  if(!firebase.profile.isLoaded) return null
  return (
    <>
    <Formik
        initialValues={{
          firstName: firebase.profile.firstName,
          lastName: firebase.profile.lastName,
          email: firebase.auth.email,
          password: '',
          confirmPassword: '',
        }}
        validationSchema={ProfileSchema}
        onSubmit={async (values, {setSubmitting}) => {
          console.log(values);
          // edit profile info...
          await editProfile(values);
          setSubmitting(false);
        }}
      >
        {({isSubmitting, isValid}) => (
          <FormWrapper>
            <Heading noMargin size='h1' color='white'>
              Edit your profile
            </Heading>
            <Heading bold size='h4' color='white'>
              Here you can edit your profile
            </Heading>
            <StyledForm>
              <Field
                type='text'
                name='firstName'
                placeholder='Your firstName...'
                component={Input}
              />
              <Field
                type='text'
                name='lastName'
                placeholder='Your lastName...'
                component={Input}
              />
              <Field
                type='email'
                name='email'
                placeholder='Your email.....'
                component={Input}
              />
              <Field
                type='password'
                name='password'
                placeholder='Your password.....'
                component={Input}
              />
              <Field
                type='password'
                name='confirmPassword'
                placeholder='Re-Type your password...'
                component={Input}
              />
              <Button 
                disabled={!isValid || isSubmitting} 
                loading={loading ? 'Editing...' : null} 
                type="submit"> 
                  Edit 
              </Button>
              <MessageWrapper>
                <Message error show={error}>{error}</Message>
              </MessageWrapper>
              <MessageWrapper>
                <Message success show={error === false}>Profile was updated!</Message>
              </MessageWrapper>
              <DeleteWrapper onClick={() => setModalOpened(true)}>Delete my account</DeleteWrapper>
            </StyledForm>
          </FormWrapper>
        )}
      </Formik>
      <Modal opened={modalOpened} close={ () => setModalOpened(false)}> 
        <Heading noMargin size='h1' color='white'>
          Delete your account
        </Heading>
        <Heading bold size='h4' color='white'>
          Do you really want to delete your account ?
        </Heading>
        <ButtonsWrapper>
          <Button 
            contain
            color="red"
            disabled={loadingDelete} 
            loading={errorDelete ? 'Deleting...' : null} 
            onClick={() => deleteUser()}> 
              Delete 
          </Button>
          <Button 
            contain
            color="main"
            onClick={() => setModalOpened(false)}> 
              Cancel 
          </Button>
        </ButtonsWrapper>
        <MessageWrapper>
          <Message error show={errorDelete}>{errorDelete}</Message>
        </MessageWrapper>
      </Modal>
      </>
  )
}

const mapStateToProps = ({firebase, auth }) => ({
  firebase,
  loading: auth.profileEdit.loading,
  error: auth.profileEdit.error,
  loadingDelete: auth.deleteUser.loading,
  errorDelete: auth.deleteUser.error,
})

const mapDispatchToProps = {
  editProfile: actions.editProfile,
  cleanUp: actions.clean,
  deleteUser: actions.deleteUser,
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
