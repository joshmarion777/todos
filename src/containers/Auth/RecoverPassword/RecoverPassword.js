import React,{useEffect} from 'react'
import { connect } from 'react-redux';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components';

// Components
import { FormWrapper, StyledForm, } from '../../../hoc/layout/elements';
import Heading from '../../../components/UI/Headings/Heading';
import Input from '../../../components/UI/Forms/Input/Input';
import Button from '../../../components/UI/Forms/Button/Button';
import Message from '../../../components/UI/Message/Message';
import * as actions from '../../../store/actions';

const MessageWrapper = styled.div`
  position: absolute;
  bottom: 0;
`;

const RecoverSchema = Yup.object().shape({
  email: Yup.string()
  .email('Invalid Email')
  .required('The email is required')
});

const RecoverPassword = ({loading, error,recoverPassword, cleanUp }) => {
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp])
  return (
    <Formik initialValues ={{
      email: '',
    }}
    validationSchema={RecoverSchema}
    onSubmit={async (values, {setSubmitting}) => {
      recoverPassword(values);
      setSubmitting(false);
    }}
    >
      {({isSubmitting, isValid}) => (
          <FormWrapper>
          <Heading noMargin size='h1' color='white'>Recover your Password</Heading>
          <Heading size='h4' bold color='white'>Type in your e-mail to recover your password</Heading>
          <StyledForm>
          <Field
              type='email'
              name='email'
              placeholder='Type your email...'
              component={Input}
              />
           <Button 
              disabled={!isValid || isSubmitting} 
              loading={loading ? 'Sending recovery email...' : null} 
              type="submit"> 
                Login 
            </Button>
          </StyledForm>
          <MessageWrapper>
            <Message error show={error}>{error}</Message>
          </MessageWrapper>
          <MessageWrapper>
            <Message success show={error === false}>Recover E-mail sent successfully</Message>
          </MessageWrapper>
        </FormWrapper>
      )}
    </Formik>
    
  )
}

const mapStateToProps = ({auth}) => ({
  loading: auth.recoverPassword.loading,
  error: auth.recoverPassword.error,
})

const mapDispatchToProps = {
  recoverPassword: actions.recoverPassword,
  cleanUp: actions.clean,
}


export default connect(mapStateToProps,mapDispatchToProps) (RecoverPassword);
