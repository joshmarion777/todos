import React, {useEffect} from 'react';
import { connect } from "react-redux";
import { Formik, Field } from "formik";
import * as Yup from 'yup';
import styled from 'styled-components';

// components
import { FormWrapper, StyledForm } from '../../../hoc/layout/elements';
import Input from '../../../components/UI/Forms/Input/Input';
import Button from '../../../components/UI/Forms/Button/Button';
import Heading from '../../../components/UI/Headings/Heading';
import Message from '../../../components/UI/Message/Message';

// redux stuff
import * as actions from '../../../store/actions';

const MessageWrapper = styled.div`
  position: absolute;
  bottom: 0;
`;

const SignUpSchema = Yup.object().shape({
  firstName: Yup.string()
  .required('Your first name is required')
  .min(3, 'Too short...')
  .max(25, 'Too Long...'),
  lastName: Yup.string()
  .required('Your last name is required')
  .min(3, 'Too short...')
  .max(25, 'Too Long...'),
  email: Yup.string()
  .email('Invalid Email')
  .required('The email is required'),
  password: Yup.string()
  .min(8, 'The password is too short')
  .required('The password is required'),
  confirmPassword: Yup.string()
  .oneOf([Yup.ref('password'), null], `Password doesn't match` )
  .required('You need to confirm your password'),
})

const SignUp = ({ signUp, loading, error, cleanUp }) => {
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp])
  console.log(loading);
  return (
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          email: '',
          password: '',
          confirmPassword: '',
        }}
        validationSchema={SignUpSchema}
        onSubmit={async (values, {setSubmitting}) => {
          console.log(values);
          await signUp(values);
          setSubmitting(false);
        }}
      >
        {({isSubmitting, isValid}) => (
          <FormWrapper>
            <Heading noMargin size='h1' color='white'>
              SignUp for an account
            </Heading>
            <Heading bold size='h4' color='white'>
              Fill in your details to register your new account
            </Heading>
            <StyledForm>
              <Field
                type='text'
                name='firstName'
                placeholder='Your firstName...'
                component={Input}
              />
              <Field
                type='text'
                name='lastName'
                placeholder='Your lastName...'
                component={Input}
              />
              <Field
                type='email'
                name='email'
                placeholder='Your email.....'
                component={Input}
              />
              <Field
                type='password'
                name='password'
                placeholder='Your password.....'
                component={Input}
              />
              <Field
                type='password'
                name='confirmPassword'
                placeholder='Re-Type your password...'
                component={Input}
              />
              <Button 
                disabled={!isValid || isSubmitting} 
                loading={loading ? 'signing up...' : null} 
                type="submit"> 
                  SignUp 
              </Button>
              <MessageWrapper>
                <Message error show={error}>{error}</Message>
              </MessageWrapper>
            </StyledForm>
          </FormWrapper>
        )}
      </Formik>
  )
}

const mapStateToProps = ({auth}) => ({
  loading: auth.loading,
  error: auth.error,
})

const mapDispatchToProps = {
  signUp: actions.signUp,
  cleanUp: actions.clean,
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);